# Image converter

CLI util to convert image or folfer with images in particular format

For linux/macOS

## Installation

* use NodeJS `>=16`
* npm install
* npm run prepare
* npm i -g
* now awailable as "image-converter" in your console

## usage

image-converter `image-or-folder-path-to-convert` `output-directory` --ext `output-images-format`

* image-or-folder-path-to-convert - reqired argument
* output-directory - './converted' by default
* output-images-format - 'webp' by default

import path from 'path';

const inputParser = (inputPath: string) => {
  const source = path.isAbsolute(inputPath)
    ? inputPath
    : path.join(process.cwd(), inputPath);

  return {
    source,
    dirName: path.dirname(source),
    isFolder: path.extname(inputPath) === '',
  };
};

const getRelativeOutputDir = (outputDir: string, inputDir: string) => {
  if (path.isAbsolute(outputDir)) {
    return outputDir;
  }
  return path.join(inputDir, outputDir);
};

export {inputParser, getRelativeOutputDir};

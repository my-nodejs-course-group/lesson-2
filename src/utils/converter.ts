import fs from 'fs/promises';
import path from 'path';
import sharp, {FormatEnum} from 'sharp';
import mkdirp from 'mkdirp';

export const convertImage = async (
  imgFilePath: string,
  outputDir: string,
  outputFormat: keyof FormatEnum
) => {
  try {
    const data = await fs.readFile(imgFilePath);
    const convertedIamge = await sharp(data).toFormat(outputFormat).toBuffer();

    const fileName = path.parse(imgFilePath).name;
    const outputPath = path.format({
      dir: outputDir,
      base: `${fileName}.${outputFormat}`,
    });
    await mkdirp(outputDir); // create directory if it's not exist
    await fs.writeFile(outputPath, convertedIamge);
  } catch (err) {
    console.log(err);
  }
};

import path from 'path';
import fs from 'fs/promises';
import {convertImage} from './converter';
import {inputParser, getRelativeOutputDir} from './pathUtils';

import {ProgramOptions, OutputFormat} from '../types';

export const convertImagesInDir = async (
  dirPath: string,
  outputDir: string,
  outputExtention: OutputFormat
) => {
  try {
    const dirItemsNames = await fs.readdir(dirPath);
    dirItemsNames.forEach((itemName: string) => {
      if (path.extname(itemName) !== '') {
        convertImage(path.join(dirPath, itemName), outputDir, outputExtention);
      }
    });
  } catch (error) {
    console.log('error: ', error);
  }
};

export const convertAction = (
  imgSource: string,
  outputDir: string,
  options: ProgramOptions
) => {
  const inputData = inputParser(imgSource);
  const relativeOutputDir = getRelativeOutputDir(outputDir, inputData.dirName);

  if (!inputData.isFolder) {
    convertImage(inputData.source, relativeOutputDir, options.ext);
  } else {
    convertImagesInDir(inputData.source, relativeOutputDir, options.ext);
  }
};

import {FormatEnum} from 'sharp';

type OutputFormat = keyof FormatEnum;

interface ProgramOptions {
  ext: OutputFormat;
}

export {ProgramOptions, OutputFormat};

#! /usr/bin/env node

import {program} from 'commander';
import {convertAction} from './utils/convertAction';

import {ProgramOptions} from './types';

program
  .option('--ext <ext>', 'Output files extention', 'webp')
  .argument('<string>', 'images source')
  .argument('[string]', 'output directory', './converted')
  .action((imgSource: string, outputDir: string, options: ProgramOptions) => {
    convertAction(imgSource, outputDir, options);
  });

program.parse();
